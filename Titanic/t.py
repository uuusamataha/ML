from sklearn import preprocessing
from sklearn.cross_validation import train_test_split, KFold, cross_val_score
from sklearn.lda import LDA

__author__ = 'H'

# Load libraries
from pandas.io.parsers import read_csv
import pandas
import numpy as np
from pandas.tools.plotting import scatter_matrix
import matplotlib.pyplot as plt
# from sklearn import model_selection
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
# from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC

url = "D:\ML\Titanic\data/train.csv"
test_url = "D:\ML\Titanic\data/test.csv"
names = ['PassengerId', 'Survived', 'Pclass', 'Name', 'Sex', 'Age', 'SibSp', 'Parch', 'Ticket', 'Fare', 'Cabin',
         'Embarked']

test_names = ['PassengerId', 'Pclass', 'Name', 'Sex', 'Age', 'SibSp', 'Parch', 'Ticket', 'Fare', 'Cabin',
         'Embarked']
def read_data(url, names):
    train_data = read_csv(url, names=names)
    le = preprocessing.LabelEncoder()
    train_data.Sex = le.fit_transform(train_data.Sex)
    train_data.Embarked = le.fit_transform(train_data.Embarked)
    train_data.Fare = le.fit_transform(train_data.Fare)
    train_data.Parch = le.fit_transform(train_data.Parch)
    train_data.SibSp = le.fit_transform(train_data.SibSp)
    train_data.Age = le.fit_transform(train_data.Age)
    train_data.Pclass = le.fit_transform(train_data.Pclass)
    train_data.PassengerId = le.fit_transform(train_data.PassengerId)
    train_data.Age = le.fit_transform(train_data.Age)
    train_data.Age = le.fit_transform(train_data.Age)
    train_data = train_data.drop(['Name','Ticket', 'Cabin'], axis=1)

    print train_data.shape
    train_data = train_data.dropna()
    train_data = train_data.sample(frac=1)
    return train_data


train_data = read_data(url, names)
# print train_data.head(5)
test_data = read_data(test_url, test_names)
# split features from labels
X_train = train_data.drop(['Survived'], axis=1).values
Y_train = train_data['Survived'].values

X_train = np.array(X_train)
Y_train = np.array(Y_train)
# print(X_train[1:50])
svc_classifer = SVC()
lda = LDA()

svc_classifer.fit(X_train, Y_train)
predictions = svc_classifer.predict(test_data)


Y_validation = read_csv("D:\ML\Titanic\data/gender_submission.csv", names=['PassengerId','Survived'])
Y_validation = Y_validation.drop(['PassengerId'], axis=1)

print(test_data.shape,'test')
print(Y_validation.shape)
print(confusion_matrix(Y_validation, predictions))
print(classification_report(Y_validation, predictions))

print(accuracy_score(Y_validation, predictions))

print(Y_validation.head(5))